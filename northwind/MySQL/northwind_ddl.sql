DROP TABLE IF EXISTS OrderDetail;  
DROP TABLE IF EXISTS SalesOrder;  
DROP TABLE IF EXISTS Product;  
DROP TABLE IF EXISTS Customer;
DROP TABLE IF EXISTS Employee;
DROP TABLE IF EXISTS Supplier;
DROP TABLE IF EXISTS Shipper;  
DROP TABLE IF EXISTS Category;

CREATE TABLE Category (
  categoryId INT AUTO_INCREMENT NOT NULL,
  categoryName VARCHAR(15) NOT NULL,
  description TEXT NULL,
  picture BLOB NULL,
  PRIMARY KEY (categoryId)  
  );

CREATE TABLE Customer (
  customerId VARCHAR(5) NOT NULL,
  companyName VARCHAR(40) NOT NULL,
  contactName VARCHAR(30) NULL,
  contactTitle VARCHAR(30) NULL,
  address VARCHAR(60) NULL,
  city VARCHAR(15) NULL,
  region VARCHAR(15) NULL,
  postalCode VARCHAR(10) NULL,
  country VARCHAR(15) NULL,
  phone VARCHAR(24) NULL,
  fax VARCHAR(24) NULL,
  PRIMARY KEY (CustomerId)                   
  );

CREATE TABLE Employee (
  employeeId INT AUTO_INCREMENT NOT NULL,
  lastname VARCHAR(20) NOT NULL,
  firstname VARCHAR(10) NOT NULL,
  title VARCHAR(30) NULL,
  titleOfCourtesy VARCHAR(25) NULL,
  birthDate DATETIME NULL,
  hireDate DATETIME NULL,
  address VARCHAR(60) NULL,
  city VARCHAR(15) NULL,
  region VARCHAR(15) NULL,
  postalCode VARCHAR(10) NULL,
  country VARCHAR(15) NULL,
  phone VARCHAR(24) NULL,
  extension VARCHAR(4) NULL,
  photo BLOB NULL,
  notes BLOB NULL,
  reportsTo INTEGER,
  photoPath VARCHAR(255) NULL,
  PRIMARY KEY (employeeId)
  );

CREATE TABLE Supplier (
  supplierId INT AUTO_INCREMENT NOT NULL,
  companyName VARCHAR(40) NOT NULL,
  contactName VARCHAR(30) NULL,
  contactTitle VARCHAR(30) NULL,
  address VARCHAR(60) NULL,
  city VARCHAR(15) NULL,
  region VARCHAR(15) NULL,
  postalCode VARCHAR(10) NULL,
  country VARCHAR(15) NULL,
  phone VARCHAR(24) NULL,
  fax VARCHAR(24) NULL,
  homePage TEXT NULL,
  PRIMARY KEY (supplierId)  
  );
  
CREATE TABLE Product (
  productId INT AUTO_INCREMENT NOT NULL,
  productName VARCHAR(40) NOT NULL,
  supplierId INT NULL,
  categoryId INT NULL,
  quantityPerUnit VARCHAR(20) NULL,
  unitPrice DECIMAL(10, 2) NULL,
  unitsInStock SMALLINT NULL,
  unitsOnOrder SMALLINT NULL,
  reorderLevel SMALLINT NULL,
  discontinued CHAR(1) NOT NULL,
  PRIMARY KEY (ProductId),  
  FOREIGN KEY (supplierId) REFERENCES Supplier(supplierId),
  FOREIGN KEY (categoryId) REFERENCES Category(categoryId)
  );

CREATE TABLE Shipper (
  shipperId INT AUTO_INCREMENT NOT NULL,
  companyName VARCHAR(40) NOT NULL,
  phone VARCHAR(44) NULL,
  PRIMARY KEY (ShipperId)
  );

CREATE TABLE SalesOrder (
  orderId INT AUTO_INCREMENT NOT NULL,
  customerId VARCHAR(5) NOT NULL,
  employeeId INT NULL,
  orderDate DATETIME NULL,
  requiredDate DATETIME NULL,
  shippedDate DATETIME NULL,
  shipperId INT NOT NULL,
  freight DECIMAL(10, 2) NULL,
  shipName VARCHAR(40) NULL,
  shipAddress VARCHAR(60) NULL,
  shipCity VARCHAR(15) NULL,
  shipRegion VARCHAR(15) NULL,
  shipPostalCode VARCHAR(10) NULL,
  shipCountry VARCHAR(15) NULL,
  PRIMARY KEY (orderId),  
  FOREIGN KEY (customerId) REFERENCES Customer(customerId), 
  FOREIGN KEY (employeeId) REFERENCES Employee(employeeId), 
  FOREIGN KEY (shipperId) REFERENCES Shipper(shipperId)
  );

CREATE TABLE OrderDetail (
   orderId INT NOT NULL,
   productId INT NOT NULL,
   unitPrice DECIMAL(10, 2) NOT NULL,
   quantity SMALLINT NOT NULL,
   discount DECIMAL(10, 2) NOT NULL,
   PRIMARY KEY (orderId, productId), 
   FOREIGN KEY (orderId) REFERENCES SalesOrder(orderId),
   FOREIGN KEY (productId) REFERENCES Product(productId) 
  );

-- Indexing & Foreign Key
CREATE INDEX IDX_OrderId_ProductId ON OrderDetail (orderId, productId);


