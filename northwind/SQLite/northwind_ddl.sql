-- Sqlite SQL script 
-- Run command below to generate the sqlite db file
-- cat northwind_core.sql | sqlite3 northwind.db


PRAGMA encoding="UTF-8";

DROP TABLE IF EXISTS Category;

CREATE TABLE Category (
  categoryId INTEGER PRIMARY KEY AUTOINCREMENT,
  categoryName VARCHAR(15) NOT NULL,
  description TEXT NULL,
  picture BLOB NULL
  );

DROP TABLE IF EXISTS Customer;

CREATE TABLE Customer (
  customerId VARCHAR(5) PRIMARY KEY,
  companyName VARCHAR(40) NOT NULL,
  contactName VARCHAR(30) NULL,
  contactTitle VARCHAR(30) NULL,
  address VARCHAR(60) NULL,
  city VARCHAR(15) NULL,
  region VARCHAR(15) NULL,
  postalCode VARCHAR(10) NULL,
  country VARCHAR(15) NULL,
  phone VARCHAR(24) NULL,
  fax VARCHAR(24) NULL
  );

DROP TABLE IF EXISTS Employee;

CREATE TABLE Employee (
  employeeId INTEGER PRIMARY KEY AUTOINCREMENT,
  lastname VARCHAR(20) NOT NULL,
  firstname VARCHAR(10) NOT NULL,
  title VARCHAR(30) NULL,
  titleOfCourtesy VARCHAR(25) NULL,
  birthDate DATETIME NULL,
  hireDate DATETIME NULL,
  address VARCHAR(60) NULL,
  city VARCHAR(15) NULL,
  region VARCHAR(15) NULL,
  postalCode VARCHAR(10) NULL,
  country VARCHAR(15) NULL,
  phone VARCHAR(24) NULL,
  extension VARCHAR(4) NULL,
  photo BLOB NULL,
  notes BLOB NULL,
  reportsTo INTEGER,
  photoPath VARCHAR(255) NULL,
  FOREIGN KEY (reportsTo) REFERENCES Employee(employeeId)
  );

DROP TABLE IF EXISTS Supplier;

CREATE TABLE Supplier (
  supplierId INTEGER PRIMARY KEY AUTOINCREMENT,
  companyName VARCHAR(40) NOT NULL,
  contactName VARCHAR(30) NULL,
  contactTitle VARCHAR(30) NULL,
  address VARCHAR(60) NULL,
  city VARCHAR(15) NULL,
  region VARCHAR(15) NULL,
  postalCode VARCHAR(10) NULL,
  country VARCHAR(15) NULL,
  phone VARCHAR(24) NULL,
  fax VARCHAR(24) NULL,
  homePage TEXT NULL
  );

CREATE TABLE Product (
  productId INTEGER PRIMARY KEY AUTOINCREMENT,
  productName VARCHAR(40) NOT NULL,
  supplierId INT NULL,
  categoryId INT NULL,
  quantityPerUnit VARCHAR(20) NULL,
  unitPrice DECIMAL(10, 2) NULL,
  unitsInStock SMALLINT NULL,
  unitsOnOrder SMALLINT NULL,
  reorderLevel SMALLINT NULL,
  discontinued CHAR(1) NOT NULL,
  FOREIGN KEY (supplierId) REFERENCES Supplier(supplierId),
  FOREIGN KEY (categoryId) REFERENCES Category(categoryId)
  );

CREATE TABLE Shipper (
  shipperId INTEGER PRIMARY KEY AUTOINCREMENT,
  companyName VARCHAR(40) NOT NULL,
  phone VARCHAR(44) NULL
  );

CREATE TABLE SalesOrder (
  orderId INTEGER PRIMARY KEY AUTOINCREMENT,
  customerId VARCHAR(5) NOT NULL,
  employeeId INT NULL,
  orderDate DATETIME NULL,
  requiredDate DATETIME NULL,
  shippedDate DATETIME NULL,
  shipperId INT NOT NULL,
  freight DECIMAL(10, 2) NULL,
  shipName VARCHAR(40) NULL,
  shipAddress VARCHAR(60) NULL,
  shipCity VARCHAR(15) NULL,
  shipRegion VARCHAR(15) NULL,
  shipPostalCode VARCHAR(10) NULL,
  shipCountry VARCHAR(15) NULL,
  FOREIGN KEY (shipperId) REFERENCES Shipper(shipperId),
  FOREIGN KEY (customerId) REFERENCES Customer(customerId), 
  FOREIGN KEY (employeeId) REFERENCES Employee(employeeId) 
  );

CREATE TABLE OrderDetail (
   orderId INT NOT NULL,
   productId INT NOT NULL,
   unitPrice DECIMAL(10, 2) NOT NULL,
   quantity SMALLINT NOT NULL,
   discount DECIMAL(10, 2) NOT NULL,
   FOREIGN KEY (orderId) REFERENCES SalesOrder(orderId),
   FOREIGN KEY (productId) REFERENCES Product(productId) 
  );


-- Indexing & Foreign Key
CREATE UNIQUE INDEX IF NOT EXISTS IDX_OrderId_ProductId ON OrderDetail (orderId, productId);


